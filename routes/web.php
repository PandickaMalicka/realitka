<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Routes for users
Route::get('/createForm', [ 'as' => 'createForm' , 'uses' => 'UserController@registrationForm']);
Route::get('/show/{id}', [ 'as' => 'show' , 'uses' => 'UserController@show']);
Route::get('/showAll', [ 'as' => 'showAll' , 'uses' => 'UserController@showAll']);
Route::post('/create', [ 'as' => 'createUser', 'uses' => 'UserController@create']);
Route::post('/update/{id}', [ 'as' => 'update', 'uses' => 'UserController@update']);
Route::get('/delete/{id}', [ 'as' => 'delete', 'uses' => 'UserController@delete']);
Route::get('/loginForm', ['as' => 'loginForm', 'uses' => 'UserController@logInForm']);


//Routes for Home
Route::get('/home', ['as' => 'home' , 'uses'=>'HomeController@index']);
Route::get('/show', ['as' => 'showAllProperties' , 'uses'=>'HomeController@show']);

//Routes about
Route::get('/about', ['as' => 'about' , 'uses'=>'AboutController@show']);

//Routes property
Route::get('/createProperty', ['as' => 'createProperty' , 'uses'=>'PropertyController@createForm']);
Route::post('/addProperty', [ 'as' => 'addProperty', 'uses' => 'PropertyController@create']);
Route::get('/property/{id}', ['as' => 'propertyShow', 'uses' => 'PropertyController@show']);

Route::get('/canEditProperty/{id}', ['as' => 'canEditProperty', 'uses' => 'PropertyController@canEditProperty']);
Route::post('/editProperty/{id}', ['as' => 'editProperty', 'uses' => 'PropertyController@editProperty']);
Route::post('/saveEditProperty/{id}', [ 'as' => 'saveEditProperty', 'uses' => 'PropertyController@saveEditProperty']);

Route::get('/canDeleteProperty/{id}', ['as' => 'canDeleteProperty', 'uses' => 'PropertyController@canDeleteProperty']);
Route::post('/deleteProperty/{id}', [ 'as' => 'deleteProperty', 'uses' => 'PropertyController@deleteProperty']);

Route::get('/login', 'HomeController@login')->name('login');


Auth::routes();

Route::post('/filterProperty', ['as' => 'filterProperty' , 'uses'=>'FilterController@filterProperty']);

