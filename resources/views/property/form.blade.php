@extends('layout');
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                  <h2> <div class="panel-heading">Pridaj property</div></h2>

                    <div class="panel-body">
    <form method="post" action="{{ action("PropertyController@create")}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        Nazov:<br>
        <input type="text" name="name" value="nazov">
        <br>
        Country:<br>
        <select name="country_id">
            @foreach($countries as $country)
                <option value={{$country->id}}>{{$country->name}}</option>
                @endforeach
        </select>
        <br>
        City:<br>
        <select name="city_id">
        @foreach($cities as $city)
            <option value={{$city->id}}>{{$city->name}}</option>
        @endforeach
        </select>
        <br>
        City area:<br>
        <select name="city_area_id">
            @foreach($city_areas as $city_area)
                <option value={{$city_area->id}}>{{$city_area->name}}</option>
            @endforeach
        </select>
        <br>
        Price:
        <br>
        <input type="number" name="price" value="100">
        <br>
        Adress:<br>
        <input type="text" name="adress" value="adress of property">
        <br>
        Room count:<br>
        <input type="number" name="room_count" value="">
        <br>
        Garage count:<br>
        <input type="number" name="garage_count" value="">
        <br>
        Area in SQ feet:<br>
        <input type="number" name="area" value="">
        <br>
        Description:<br>
        <input type="text" name="description" value="Enter description here">
        <br>
        Password for Editation:<br>
        <input type="password" name="password" >
        <br>

        <br>
        <input type="submit" value="Ulozit">
    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection