@extends('layout')

@section('content')
    <div class="row recent_row">
        <div class="col">
            <div class="recent_slider_container">
                <div class="owl-carousel owl-theme recent_slider">

                    <!-- Slide -->
                    @foreach($properties as $property)
                        <div class="owl-item">
                            <div class="recent_item">
                                <div class="recent_item_inner">
                                    <div class="recent_item_image">
                                        <img src="images/property_1.jpg" alt="">
                                        <div class="tag_featured property_tag"><a href="#">Featured</a></div>
                                    </div>
                                    <div class="recent_item_body text-center">
                                        <div class="recent_item_location">{{$property->name}}</div>
                                        <div class="recent_item_title"><a href="property.html">{{$property->name}}</a></div>
                                        <div class="recent_item_price">{{$property->price}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="button recent_button"><a href={{action('PropertyController@show',['id' => $property->id])}}>see more</a></div>
                    @endforeach


                </div>

                <div class="recent_slider_nav_container d-flex flex-row align-items-start justify-content-start">
                    <div class="recent_slider_nav recent_slider_prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>
                    <div class="recent_slider_nav recent_slider_next"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>
                </div>
            </div>

        </div>
    </div>


@endsection
