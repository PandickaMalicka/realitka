@extends('layout');
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>

                    <div class="panel-body">
                        <form method="post" action="{{ action("PropertyController@saveEditProperty",['id'=>$property->id])}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            Nazov:<br>
                            <input type="text" name="name" value={{$property->name}}>
                            <br>
                            Country:<br>
                            <select name="country_id">
                                @foreach($countries as $country)
                                    @if($country->id == $property->country_id)
                                        <option selected="selected" value={{$country->id}}>{{$country->name}}</option>
                                    @else
                                    <option value={{$country->id}}>{{$country->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <br>
                            City:<br>
                            <select name="city_id">
                                @foreach($cities as $city)
                                    @if($city->id == $property->city_id)
                                        <option selected="selected"  value={{$city->id}}>{{$city->name}}</option>
                                    @else
                                    <option value={{$city->id}}>{{$city->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <br>
                            City area:<br>
                            <select name="city_area_id">
                                @foreach($city_areas as $city_area)
                                    @if($city_area->id == $property->city_area_id)
                                        <option selected="selected" value={{$city_area->id}}>{{$city_area->name}}</option>
                                    @else
                                        <option value={{$city_area->id}}>{{$city_area->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <br>
                            Price:
                            <br>
                            <input type="number" name="price" value={{$property->price}}>
                            <br>
                            Adress:<br>
                            <input type="text" name="adress" value={{$property->adress}}>
                            <br>
                            Room count:<br>
                            <input type="number" name="room_count" value={{$property->room_count}}>
                            <br>
                            Garage count:<br>
                            <input type="number" name="garage_count" value={{$property->garage_count}}>
                            <br>
                            Area in SQ feet:<br>
                            <input type="number" name="area" value={{$property->area}}>
                            <br>
                            Description:<br>
                            <input type="text" name="description" value={{$property->description}}>
                            <br>

                            <br>
                            <input type="submit" value="Ulozit">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection