@extends('layout')

@section('content')




                <!-- Property Content -->
                <div class="col-lg-7 offset-lg-1">
                    <div class="property_content">
                        <div class="property_icons">
                            <div class="property_title">Extra Facilities</div>
                            <div class="property_text property_text_1">
                                <p>Donec ullamcorper nulla non metus auctor fringi lla. Curabitur blandit tempus porttitor.</p>
                            </div>
                            <div class="property_rooms d-flex flex-sm-row flex-column align-items-start justify-content-start">

                                <!-- Property Room Item -->
                                <div class="property_room">
                                    <div class="property_room_title">Bedrooms</div>
                                    <div class="property_room_content d-flex flex-row align-items-center justify-content-start">
                                        <div class="room_icon"><img src="{{asset('img/room_1.png')}}" alt=""></div>
                                        <div class="room_num">{{$property->room_count}}</div>
                                    </div>
                                </div>

                                <!-- Property Room Item -->
                                <div class="property_room">
                                    <div class="property_room_title">Area</div>
                                    <div class="property_room_content d-flex flex-row align-items-center justify-content-start">
                                        <div class="room_icon"><img src="{{asset('img/room_3.png')}}" alt=""></div>

                                        <div class="room_num">{{$property->area}} Sq Ft</div>
                                    </div>
                                </div>

                                <!-- Property Room Item -->
                                <div class="property_room">
                                    <div class="property_room_title">Garage</div>
                                    <div class="property_room_content d-flex flex-row align-items-center justify-content-start">
                                        <div class="room_icon"><img src="{{asset('img/room_5.png')}}" alt=""></div>
                                        <div class="room_num">{{$property->garage_count}}</div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- Description -->

                        <div class="property_description">
                            <div class="property_title">Description</div>
                            <div class="property_text property_text_2">
                                {{$property->descriptions}}
                            </div>
                        </div>

                        <a href="{{action('PropertyController@canEditProperty', ['id' => $property->id])}}">Edit</a>
                        <a href="{{action('PropertyController@canDeleteProperty', ['id' => $property->id])}}">Delete</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection