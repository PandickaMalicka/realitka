<?php
/**
 * Created by PhpStorm.
 * User: BE01
 * Date: 17. 10. 2018
 * Time: 20:15
 */
?>

<form method="post" action="{{ action("UserController@update", ['id' => $user->id]) }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    Meno:<br>
    <input type="text" name="firstname" value="{{$user->name}}">
    <br>
    Priezvisko:<br>
    <input type="text" name="lastname" value="${{$user->surname}}">
    <br>
    Email:<br>
    <input type="text" name="email" value="{{$user->email}}">
    <br>
    Vek:<br>
    <input type="number" name="age" value="{{$user->age}}">
    <br><br>
    <input type="submit" value="Ulozit">
</form>
