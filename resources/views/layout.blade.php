<!DOCTYPE html>
<html lang="en">
<head>
<title>Bluesky</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Bluesky template project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href={{asset('assets/css/style.css')}}>
</head>
<body>

<div class="super_container">

	<!-- Header -->

	<header class="header">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_content d-flex flex-row align-items-center justify-content-start">
						<div class="logo">
							<a href="{{Route('home')}}"><img src="{{asset('img/logo.png')}}" alt=""></a>
						</div>
						<nav class="main_nav">
							<ul>
								<li class="active"><a href="{{Route('home')}}">Home</a></li>
								<li><a href="{{Route('createProperty')}}">Add Properties</a></li>
								<li><a href="{{Route('createForm')}}">Register</a></li>
								@if(\Illuminate\Support\Facades\Auth::check())
									<li>{{\Illuminate\Support\Facades\Auth::user()->name}}</li>
									@else
								<li><a href="{{Route('login')}}">Log in</a></li>
								@endif
							</ul>
						</nav>
						<div class="phone_num ml-auto">
							<div class="phone_num_inner">
								<img src="{{asset('img/phone.png')}}" alt=""><span>652-345 3222 11</span>
							</div>
						</div>
						<div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<h1>
<b>Filter:</b>
	</h1>
@include('filter')
	<br>

<div class="content">
	@yield('content')
</div>



<footer class="footer">
		<div class="footer_main">
			<div class="container">
				<div class="row">
					<div class="col-lg-3">
						<div class="footer_logo"><a href="{{Route('home')}}"><img src="{{asset('img/logo_large.png')}}" alt=""></a></div>
					</div>
					<div class="col-lg-9 d-flex flex-column align-items-start justify-content-end">
						<div class="footer_title">Latest Properties</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 footer_col">
						<div class="footer_about">
							<div class="footer_about_text">Donec in tempus leo. Aenean ultricies mauris sed quam lacinia lobortis. Cras ut vestibulum enim, in gravida nulla. Curab itur ornare nisl at sagittis cursus.</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="footer_bar">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="footer_bar_content d-flex flex-row align-items-center justify-content-start">
							<div class="cr"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</div>
							<div class="footer_nav">
								<ul>
                                    <li class="active"><a href="{{Route('home')}}">Home</a></li>
                                    <li><a href="{{Route('about')}}">About us</a></li>
                                    <li><a href="{{Route('createProperty')}}">Add Properties</a></li>
                                    <li><a href="{{Route('createForm')}}">Register</a></li>
                                    @if(\Illuminate\Support\Facades\Auth::check())
                                        <li>{{\Illuminate\Support\Facades\Auth::user()->name}}</li>
                                    @else
                                        <li><a href="{{Route('login')}}">Log in</a></li>
                                    @endif
								</ul>
							</div>
							<div class="footer_phone ml-auto"><span>call us: </span>652 345 3222 11</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
</div>

<script src="{{asset('assets/js/script.js')}}"></script>
</body>
</html>