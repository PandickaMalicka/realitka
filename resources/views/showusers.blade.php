<?php
/**
 * Created by PhpStorm.
 * User: BE01
 * Date: 17. 10. 2018
 * Time: 20:08
 */
?>

<table>
    @foreach($users as $user)
        <tr>
            <td>
                {{$user->name}} {{$user->surname}}
            </td>
            <td>
                {{$user->email}}
            </td>
            <td>
                {{$user->age}}
            </td>
            <td>
                <a href="{{ action("UserController@show",['id' => $user->id]) }}">editovat</a>
            </td>
        </tr>
    @endforeach
</table>
