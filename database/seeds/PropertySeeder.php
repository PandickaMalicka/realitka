<?php

use Illuminate\Database\Seeder;

class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('properties')->insert([
            'name'=>"Chrenova",
            'country_id'=>1,
            'cities_id'=>1,
            'city_area_id'=>1,
            'price'=>100000,
            'adress'=>"vystavna 50",
            'description'=>"Chrenova is the place",
            'room_count'=>3,
            'garage_count'=>0,
            'area'=>80,
            'users_id'=>1,
            'password'=>bcrypt('asdfg')
        ]);

        DB::table('properties')->insert([
            'name'=>"Californication",
            'country_id'=>2,
            'cities_id'=>2,
            'city_area_id'=>2,
            'price'=>900000,
            'adress'=>"californska 50",
            'description'=>"california is the place",
            'room_count'=>10,
            'garage_count'=>2,
            'area'=>200,
            'users_id'=>1,
            'password'=>bcrypt('asdfg')
        ]);
    }
}