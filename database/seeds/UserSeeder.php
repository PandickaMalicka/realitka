<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>"Denis",
            'lastname'=>"Fuzik",
            'email' => "qwer@gmail.com",
            'password' => bcrypt('asdfgh'),
            'phone' => 1337,
            'users_type_id' => 2
        ]);

        DB::table('users')->insert([
            'name'=>"realitka",
            'lastname'=>"ztrjeme",
            'email' => "realitka@gmail.com",
            'password' => bcrypt('asdfgh'),
            'phone' => 1337,
            'DIC' => 206549,
            'ICO' => 20654620,
            'users_type_id' => 2
        ]);
    }
}
