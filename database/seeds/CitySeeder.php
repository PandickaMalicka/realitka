<?php

use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            'name'=>"Nitra",
            'countries_id'=>1
        ]);

        DB::table('cities')->insert([
            'name'=>"California",
            'countries_id'=>2
        ]);
    }
}