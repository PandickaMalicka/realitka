<?php

use Illuminate\Database\Seeder;

class Users_typeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_type')->insert([
            'type'=>"user"
        ]);

        DB::table('users_type')->insert([
            'type'=>"realitka"
        ]);
    }
}