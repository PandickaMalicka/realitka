<?php

use Illuminate\Database\Seeder;

class City_areaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('city_area')->insert([
            'name'=>"Chrenova",
            'cities_id'=>1
        ]);

        DB::table('city_area')->insert([
            'name'=>"california 1",
            'cities_id'=>2
        ]);
    }
}