<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name','50')->nullable($value = true);
            $table->string('lastname','50')->nullable($value = true);
            $table->string('email','50') ->unique();
            $table->string('password');
            $table->unsignedInteger('DIC')->nullable($value = true);
            $table->unsignedInteger('ICO')->nullable($value = true);
            $table->string('phone','15') ->nullable($value = true);
            $table->integer('users_type_id')->unsigned();
            $table->foreign('users_type_id')->references('id')->on('users_type');
            $table->string('remember_token','100')->nullable($value= true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
