<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable($value = true);
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->integer('cities_id')->unsigned();
            $table->foreign('cities_id')->references('id')->on('cities');
            $table->integer('city_area_id')->unsigned();
            $table->foreign('city_area_id')->references('id')->on('city_area');
            $table->integer('users_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users')->nullable($value = true);
            $table->integer('price');
            $table->string('adress','250');
            $table->text('description')->nullable($value = true);
            $table->integer('room_count');
            $table->integer('garage_count');
            $table->integer('area');
            $table->string('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
