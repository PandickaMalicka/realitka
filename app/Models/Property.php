<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{

    protected  $table='properties';

    protected  $fillable=['name','country_id', 'cities_id' , 'city_area_id' , 'users_id' , 'price', 'adress','description', 'room_count', 'garage_count', 'area','password'];

    public function country(){
        return $this->hasOne('App\Models\Country');
    }

    public function cityArea(){
        return $this->hasOne('App\Models\City_area');
    }
    public function user(){
        return $this->hasOne('App\Models\User');
    }
}
