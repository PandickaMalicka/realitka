<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City_area extends Model
{
    protected  $table='city_area';

    protected  $fillable=['name','cities_id'];

    public function city(){
        return $this->hasOne('App\Models\City');
    }
}
