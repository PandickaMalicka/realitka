<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    //
    protected $table = 'users';

    protected $fillable =['name', 'lastname','email', 'password', 'dic',
        'ico','phone', 'users_type_id'];

    public function usersType(){
        return $this->hasOne('App\Models\User_type');
    }
}
