<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected  $table='cities';

    protected  $fillable=['name','countries_id'];

    public function country(){
        return $this->hasOne('App\Models\Country');
    }

    public function property(){
        return $this->belongsTo('App\Models\Property');
    }
}
