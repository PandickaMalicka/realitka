<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Property;
use Illuminate\Http\Request;

class FilterController extends Controller
{

    public function filterProperty(Request $request)
    {
        $properties = Property::all();
        $name = $request->input('name');
        if($request->input('city') != ""){
            $city = City::where( 'name' ,'like', $request->input('city'))->first();
        }else{
            $city = null;
        }

        $minprice = $request->input('minprice');
        $maxprice = $request->input('maxprice');
        $room_count = $request->input('room_count');
        $area = $request->input('area');

        if(isset($name)){
            $properties = $properties->where('name','like', $name);
        }
        if($city != null){
            $properties = $properties->where('cities_id','=', $city->id);
        }
        if(isset($minprice)){
            $properties = $properties->where('price','>=',$minprice);
        }
        if(isset($maxprice)){
            $properties = $properties->where('price','<=',$maxprice);
        }
        if(isset($room_count)){
            $properties = $properties->where('room_count','>=',$room_count);
        }
        if(isset($area)){
            $properties = $properties->where('area','>=',$area);
        }
        return view('property.browse',compact('properties'));
         }
}
