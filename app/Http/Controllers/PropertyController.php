<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\City_area;
use App\Models\Country;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Property;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class PropertyController extends Controller
{
    //

    function  createForm(){
        $countries = Country::all();
        $city_areas =City_area::all();
        $cities = City::all();
        return view('property/form' ,compact('countries','city_areas','cities'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function create(Request $request){
        $property = new Property();
        $property->name = $request->input('name');
        $property->country_id = $request->input('country_id');
        $property->cities_id = $request->input('city_id');
        $property->city_area_id = $request->input('city_area_id');
        $property->price = $request->input('price');
        $property->adress = $request->input('adress');
        $property->description = $request->input('description');
        $property->room_count = $request->input('room_count');
        $property->garage_count = $request->input('garage_count');
        $property->area = $request->input('area');
        $property->password = bcrypt($request->input('password'));
        if(Auth::check()){
            $property->users_id = Auth::id();
        }else{
            $property->users_id = 1;
        }
        $property->save();

        return redirect()->route('home');
    }

    function show($id){
        $property = Property::where("id", "=", $id)->first();
        return view('property.show_detail',compact('property'));
    }

    function canEditProperty($id)
    {
        $property = Property::where("id", "=", $id)->first();
            return view('property.validateEdit',compact('property'));
    }

    function editProperty($id, Request $request){
        $property = Property::where("id", "=", $id)->first();
        if(Hash::check($request->input('password'), $property->password)){
        $countries = Country::all();
            $city_areas =City_area::all();
            $cities = City::all();
            return view('property.editProperty', compact('property','countries','city_areas','cities'));
        }else{
            return view('home');
        }
    }

    function saveEditProperty($id, Request $request){
        $property = Property::where("id", "=", $id)->first();
        $property->name = $request->input('name');
        $property->country_id = $request->input('country_id');
        $property->cities_id = $request->input('city_id');
        $property->city_area_id = $request->input('city_area_id');
        $property->price = $request->input('price');
        $property->adress = $request->input('adress');
        $property->description = $request->input('description');
        $property->room_count = $request->input('room_count');
        $property->garage_count = $request->input('garage_count');
        $property->area = $request->input('area');
        $property->save();
       /* $property->update(['name'=>$request->input('name'),
            'country_id'=>$request->input('country_id'),
            'cities_id'=>$request->input('city_id'),
            'city_area_id'=>$request->input('city_area_id'),
            'price'=>$request->input('price'),
            'adress'=>$request->input('adress'),
            'description'=>$request->input('description'),
            'room_count'=>$request->input('room_count'),
            'garage_count'=>$request->input('garage_count'),
            'area'=>$request->input('area')
            ]);*/

        return redirect()->action('HomeController@index');

    }

    function canDeleteProperty($id){
        $property = Property::where("id", "=", $id)->first();

        return view('property.validateDelete',compact('property'));
    }

    function deleteProperty($id, Request $request){
        $property = Property::where("id", "=", $id)->first();
        if(Hash::check($request->input('password'), $property->password)) {

        $property->delete();
        return view('home');
        }
    }
}
