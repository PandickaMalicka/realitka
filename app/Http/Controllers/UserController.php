<?php

namespace App\Http\Controllers;

use App\Models\User_type;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\View\View;
use function Sodium\compare;

class UserController extends Controller
{
    public function show($id)
    {
        $user = User::findoOrFail($id);
        return view('updateuser', ['user' => $user]);
    }

    public function create(Request $request)
    {
        $user = new User();
        $user->name = $request->input('name');
        $user->lastname = $request->input('lastname');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->dic = $request->input('dic');
        $user->ico = $request->input('ico');
        $user->phone = $request->input('phone');
        $user->users_type_id = $request->input('user_type');
        $user->save();

        return response()->view('home');

    }

    public function update($id, Request $request)
    {
        $user = User::where("id", "=", $id)->first();
        $user->update(["age" => $request->input('age'),
            "name" => $request->input('firstname'),
            "surname" => $request->input('lastname'),
            "email" => $request->input('email')]);

        return redirect()->action('UserController@showAll');
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();
    }

    public function showAll()
    {
        $users = User::all();
        return view('showusers', ['users' => $users]);
    }

    public function registrationForm()
    {
        $user_types = User_type::all();
        return view('register_form',compact('user_types'));
    }
}
