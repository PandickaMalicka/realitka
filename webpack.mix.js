let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts(['resources/assets/js/app.js',
'resources/assets/js/about.js',
'resources/assets/js/bootstrap.js',
'resources/assets/js/contact.js',
'resources/assets/js/custom.js',
'resources/assets/js/elements.js',
'resources/assets/js/jquery-3.2.1.min.js',
'resources/assets/js/news.js',
'resources/assets/js/properties.js',
'resources/assets/js/property.js'], 'public/assets/js/script.js');
mix.styles(['resources/assets/css/about.css',
'resources/assets/css/about_responsive.css',
'resources/assets/css/contact.css',
'resources/assets/css/contact_responsive.css',
'resources/assets/css/elements.css',
'resources/assets/css/elements_responsive.css',
'resources/assets/css/main_styles.css',
'resources/assets/css/news.css',
'resources/assets/css/news_responsive.css',
'resources/assets/css/properties.css',
'resources/assets/css/properties_responsive.css',
'resources/assets/css/property.css',
'resources/assets/css/property_responsive.css',
'resources/assets/css/responsive.css'], 'public/assets/css/style.css');
mix.sass('resources/assets/sass/app.scss', 'public/assets/css/style.css');
